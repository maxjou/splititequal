import createReducer from '../lib/createReducer'
import * as types from '../actions/types'

export const getAllUsers = createReducer({},{
    [types.Set_All_USERS](state,action){
        let newState ={}
        action.users.forEach((user) => {
            newState[user.id] = user
        })
        return newState;
    }


})

export const usersCount = createReducer(0,{
    [types.Set_All_USERS](state,action) {
        return action.users.length;
    }
})