import * as types from './types'
import Api from "../lib/api";

// export function getAllUsers(){
//     return (dispatch, getState) =>{
//             console.log("hej")
//         return Api.get('/users').then( response => {
//             dispatch(setAllUsers({users: response}))
//             console.log("hejehj")
//         }).catch((ex) => {
//             console.log(ex)
//         })
//
//     }
// }

export function getAllUsers(){
    return (dispatch, getState) =>{
        return  fetch('https://jsonplaceholder.typicode.com/users')
            .then(response => response.json())
            .then(json => dispatch(setAllUsers({users: json})))
    }
}


export function setAllUsers({users}){

    return {
        type: types.Set_All_USERS,
        users
    }
}