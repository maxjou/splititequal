import React from 'react'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import { ActionCreators} from "../actions";
import AppNavigator from '../navigation/AppNavigator'
import {StyleSheet, View, Text, TouchableHighlight, ListView} from 'react-native';
import MainTabNavigator from '../navigation/MainTabNavigator'
import HomeScreen from '../screens/HomeScreen'


class AppContainer extends React.Component {

    render(){
        return (
            <View style={styles.container}>
                <HomeScreen {...this.props}/>
            </View>
        );
    }

}
const styles = StyleSheet.create({
    container: {
        marginTop: 50,
        flex: 1,
        backgroundColor: '#fff',
    },
});

function mapDispatchToProps(dispatch) {
    return bindActionCreators(ActionCreators,dispatch)

}


export default connect((state) => {return {} }, mapDispatchToProps)(AppContainer);