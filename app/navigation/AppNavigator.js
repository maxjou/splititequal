import React from 'react';
import { createSwitchNavigator } from 'react-navigation';

import MainTabNavigator from './MainTabNavigator';
import {connect} from "react-redux";

export default createSwitchNavigator({
    // You could add another route here for authentication.
    // Read more at https://reactnavigation.org/docs/en/auth-flow.html
    Main: MainTabNavigator,
});

// function mapStateToProps(state){
//     return {
//         searchedUsers: state.searchedUsers
//     }
// }
//
// export default connect(mapStateToProps)(createSwitchNavigator);