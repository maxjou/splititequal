package com.project.splititequal.group;


import lombok.*;

import java.util.Set;

@Getter
@Setter
@Data
@ToString
@AllArgsConstructor
public class GroupModel {

    private Long id;
    private String groupName;
    private Set<Long> participants;
    private Set<Long> list_id;

    public GroupModel(Group group) {
        setGroupName(group.getGroupName());
        setParticipants(group.getParticipants());
    }
}
