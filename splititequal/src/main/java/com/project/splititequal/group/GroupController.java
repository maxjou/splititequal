package com.project.splititequal.group;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value ="/api/group")
public class GroupController {

    @Autowired
    private GroupService groupService;

    @GetMapping
    public List<GroupModel> getAllGroup(){
        return groupService.getAllGroups();
    }

    @PostMapping
    public GroupModel createNewGroup(@RequestBody GroupModel groupModel){
        return groupService.createGroup(groupModel);
    }

    @PutMapping
    public ResponseEntity<GroupModel> updateGroup(@RequestBody GroupModel groupModel){
       return new ResponseEntity<>(groupService.updateGroup(groupModel), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteGroupById(@PathVariable Long id){
        return groupService.deleteGroupById(id);
    }



}
