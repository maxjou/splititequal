package com.project.splititequal.group;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class GroupService {

    @Autowired
    private GroupRepository groupRepository;

    public List<GroupModel> getAllGroups() {
        return convertGroupListToGroupModelList(groupRepository.findAll());
    }

    public GroupModel createGroup(GroupModel groupModel) {
        Group group = new Group();
        group.setGroupName(groupModel.getGroupName());
        group.setParticipants(groupModel.getParticipants());
        return new GroupModel(groupRepository.save(group));
    }

    public GroupModel updateGroup(GroupModel groupModel) {
        Group group = groupRepository.getOne(groupModel.getId());
        group.setGroupName(groupModel.getGroupName());
        group.setParticipants(groupModel.getParticipants());

        return new GroupModel(groupRepository.save(group));

    }

    public ResponseEntity<Void> deleteGroupById(Long id) {
        if (id != null) {
            groupRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    public List<GroupModel> convertGroupListToGroupModelList(List<Group> groupList){
        List<GroupModel>groupModelList = new ArrayList<>();

        for(Group group : groupList){
            groupModelList.add(new GroupModel(group));
        }
        return groupModelList;

    }

}
