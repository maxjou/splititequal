package com.project.splititequal.group;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.Set;

@Getter
@Setter
@ToString
@Entity
@Table(name = "user_group")
public class Group {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name ="group_name")
    private String groupName;

    @ElementCollection
    @CollectionTable(name="participants_list", joinColumns = @JoinColumn(name="group_id"))
    private Set<Long> participants;

    @ElementCollection
    @CollectionTable(name="lists_in_group", joinColumns = @JoinColumn(name="group_id"))
    private Set<Long> list_id;

}
