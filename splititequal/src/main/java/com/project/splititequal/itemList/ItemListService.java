package com.project.splititequal.itemList;

import com.project.splititequal.group.Group;
import com.project.splititequal.group.GroupRepository;
import com.project.splititequal.item.Item;
import com.project.splititequal.item.ItemRepository;
import com.project.splititequal.user.User;
import com.project.splititequal.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class ItemListService {

    @Autowired
    private ItemListRepository itemListRepository;


    @Autowired
    ItemRepository itemRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    GroupRepository groupRepository;


    public List<ItemListModel> getAllItemLists() {
        return convertItemListToItemListModel(itemListRepository.findAll());
    }

    public ItemListModel createItemList(ItemListModel itemListModel) {

        ItemList itemList = new ItemList();
        itemList.setItem_id(itemListModel.getItem_id());
        itemList.setListName(itemListModel.getListName());
        itemList.setGroupId(itemListModel.getGroupId());
        itemList.setCreatorId(itemListModel.getCreatorId());

        return new ItemListModel(itemListRepository.save(itemList));
    }

    public ItemListModel updateItemList(ItemListModel itemListModel){
        ItemList itemList = new ItemList();
        itemList.setId(itemListModel.getId());
        itemList.setListName(itemListModel.getListName());
        itemList.setGroupId(itemListModel.getGroupId());
        itemList.setCreatorId(itemListModel.getCreatorId());
        return new ItemListModel(itemListRepository.save(itemList));
    }

    public ResponseEntity<Void> deleteItemListById(Long id){
        if(id != null){
            itemListRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.OK);
        }else{
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    public List<ItemListModel> convertItemListToItemListModel(List<ItemList> itemLists){
        List<ItemListModel>itemListModels = new ArrayList<>();

        for(ItemList item : itemLists){
            itemListModels.add(new ItemListModel(item));
        }
        return itemListModels;

    }

    public List<String> owe(Long listId){
        Optional<ItemList> il = itemListRepository.findById(listId);
        Optional<Group> group = groupRepository.findById(il.get().getGroupId());

        Set<Long> participants = group.get().getParticipants();

        Set<Long> items = il.get().getItem_id();
//        items.add(1L);
//        items.add(2L);
//        items.add(3L);

        Map<Long, Double> users = new HashMap<>();
        Double total = 0D;
        for(Long id : items){
            Optional<Item> item = itemRepository.findById(id);
            total += item.get().getCost();
            if(users.containsKey(item.get().getUserId())){
                double temp = (users.get(item.get().getUserId()) + item.get().getCost());

                users.put(item.get().getUserId(), temp);
            }
            else{
                users.put(item.get().getUserId(), item.get().getCost());

            }
        }

        List<String> owe = new ArrayList<>();
        for(Long part : participants){

            Optional<User> user = userRepository.findById(part);
            Double penga = total / participants.size();

            if(users.containsKey(part)){
                penga -= users.get(part);
            }
            System.out.println(penga);
            owe.add(user.get().getId() + "/" + user.get().getUserName() + "/" + penga);
        }
        System.out.println(users);
        return owe;
    }

}
