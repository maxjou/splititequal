package com.project.splititequal.itemList;


import lombok.*;

import java.util.Set;

@Getter
@Setter
@Data
@ToString
@AllArgsConstructor
public class ItemListModel {

    private Long id;
    private String listName;
    private Long groupId;
    private Long creatorId;
    private Set<Long> item_id;

    public ItemListModel(ItemList itemList) {
        setListName(itemList.getListName());
        setGroupId(itemList.getGroupId());
        setCreatorId(itemList.getCreatorId());
        setItem_id(itemList.getItem_id());
    }
}
