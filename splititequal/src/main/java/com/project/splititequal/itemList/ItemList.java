package com.project.splititequal.itemList;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.Set;

@Getter
@Setter
@ToString
@Entity
@NoArgsConstructor
public class ItemList {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "listName")
    private String listName;

    @Column(name = "groupId")
    private Long groupId;

    @Column(name = "creatorId")
    private Long creatorId;

}
