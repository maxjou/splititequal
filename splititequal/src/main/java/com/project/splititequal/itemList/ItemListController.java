package com.project.splititequal.itemList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping(value = "/api/list")
public class ItemListController {

    @Autowired
    private ItemListService itemListService;

    @PostMapping
    public ItemListModel createList(@RequestBody ItemListModel itemListModel){
        return itemListService.createItemList(itemListModel);
    }

    @GetMapping
    public List<ItemListModel> getItemList(){
        return itemListService.getAllItemLists();
    }

    @PutMapping
    public ResponseEntity<ItemListModel> updateItemList(@RequestBody ItemListModel itemListModel){
        return new ResponseEntity<>(itemListService.updateItemList(itemListModel), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteItemListById(@PathVariable Long id){
        return itemListService.deleteItemListById(id);

    }



    @GetMapping(value = "/owe/{listid}")
    public List<String> getOwe(@PathVariable Long listid){
        return itemListService.owe(listid);
    }

}
