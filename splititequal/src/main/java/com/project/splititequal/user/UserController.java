package com.project.splititequal.user;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/user")
public class UserController {


    /**
     * Crud uses only user path
     */


    @Autowired
    private UserService userService;

    @PostMapping
    public User createNewUser(@RequestBody User user){
        return userService.createUser(user);
    }

    @GetMapping
    public List<User> getUser(){
        return userService.getAllUsers();
    }

}
