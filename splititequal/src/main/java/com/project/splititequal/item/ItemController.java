package com.project.splititequal.item;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/item")
public class ItemController {
    @Autowired
    ItemService itemService;

    @PostMapping
    public ItemModel postItem(@RequestBody ItemModel item){
        return itemService.createItem(item);
    }

    @GetMapping
    public List<ItemModel> getItem(){
        return itemService.getAllItems();
    }

    @GetMapping(value = "/useritems/{userid}")
    public List<ItemModel> getUserItems(@PathVariable Long userid ){
        return itemService.getItemsByUser(userid);
    }

    @GetMapping(value = "/listitems/{listid}")
    public List<ItemModel> getListItems(@PathVariable Long listid ){
        return itemService.getListItems(listid);
    }

    @PutMapping
    public ResponseEntity<ItemModel> updateItem(@RequestBody ItemModel itemModel){
        return new ResponseEntity<>(itemService.updateItem(itemModel), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteItemById (@PathVariable Long id ){
        return itemService.deleteItemById(id);
    }

}
