package com.project.splititequal.item;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ItemRepository extends JpaRepository<Item, Long> {
    List<ItemModel> findAllByUserId(Long userId);

    List<ItemModel> findAllByListId(Long listId);
}
