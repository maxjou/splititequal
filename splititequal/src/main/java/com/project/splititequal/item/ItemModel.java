package com.project.splititequal.item;


import lombok.*;

@Getter
@Setter
@Data
@ToString
@AllArgsConstructor
public class ItemModel {

    private Long id;

    private Double cost;
    private String name;
    private Long listId;
    private Long userId;
    private String description;

    public ItemModel(Item item) {
        setCost(item.getCost());
        setName(item.getName());
        setListId(item.getListId());
        setUserId(item.getUserId());
        setDescription(item.getDescription());
    }
}
