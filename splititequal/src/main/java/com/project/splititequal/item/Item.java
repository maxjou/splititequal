package com.project.splititequal.item;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.Set;

@Getter
@Setter
@ToString
@NoArgsConstructor
@Entity
public class Item {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Double cost;
    private String name;
    private Long listId;
    private Long userId;

    @Column(columnDefinition="text")
    private String description;

    @ElementCollection
    @CollectionTable(name="excludedUsers", joinColumns=@JoinColumn(name="item_id"))
    private Set<Long> excludedUsers;
}
