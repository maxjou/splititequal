package com.project.splititequal.item;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ItemService {
    @Autowired
    ItemRepository itemRepo;


    public ItemModel createItem(ItemModel itemModel) {
        Item item = new Item();
        item.setCost(itemModel.getCost());
        item.setName(itemModel.getName());
        item.setListId(itemModel.getListId());
        item.setUserId(itemModel.getUserId());

        return new ItemModel(itemRepo.save(item));
    }

    public List<ItemModel> getAllItems() {
        return convertItemListToItemModelList(itemRepo.findAll());
    }

    public List<ItemModel> getItemsByUser(Long userId){
        return itemRepo.findAllByUserId(userId);
    }

    public List<ItemModel> getListItems(Long listId){
        return itemRepo.findAllByListId(listId);
    }

    public ResponseEntity<Void> deleteItemById(Long id) {
        if(id != null){
            itemRepo.deleteById(id);
            return new ResponseEntity<>(HttpStatus.OK);
        }else{
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    public ItemModel updateItem(ItemModel itemModel){
        Item item = itemRepo.getOne(itemModel.getId());
        item.setCost(itemModel.getCost());
        item.setName(itemModel.getName());
        item.setListId(itemModel.getListId());
        item.setUserId(itemModel.getUserId());
        return new ItemModel(itemRepo.save(item));
    }

    public List<ItemModel> convertItemListToItemModelList(List<Item> itemList){
        List<ItemModel>itemModelList = new ArrayList<>();

        for(Item item : itemList){
            itemModelList.add(new ItemModel(item));
        }
        return itemModelList;

    }
}
