package com.project.splititequal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class SplititequalApplication extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
 		return application.sources(SplititequalApplication.class);
	}

	public static void main(String[] args) {
		SpringApplication.run(SplititequalApplication.class, args);
	}

	private static Class<SplititequalApplication> applicationClass = SplititequalApplication.class;

}
